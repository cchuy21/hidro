<!DOCTYPE HTML>
<html>
	<head>
		<title>Servicios | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="index is-preload">
		<div id="page-wrapper">
			<?php
                $titulo="servicios";
                $show=0;
				$menu=array(array("Inicio","index",0),array("Servicios","servicios",1),array("Proyectos","proyectos",0),array("Equipo","equipo",0),array("Productos","productos",0),array("Contacto","contact",0));
				include 'pages/header.php';
			?>
            <article id="main">
            <section class="container special">
                <div class="row gtr-50" style="text-align:center">
                    <div class="col-4 col-12-narrower" style="height:150px;">
                        <div style="background-repeat: no-repeat;padding: 20.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/servicios/1.webp);height: 100%;width: 100%;">
                            <p style="margin: 0;text-align:center;color:white"><b>Sistemas de Riego</b><br />Proyecto, suministros e instalación</p>
                        </div>
                    </div>
                    <div class="col-4 col-12-narrower" style="height:150px;">
                        <div style="background-repeat: no-repeat;padding: 20.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/servicios/2.webp);height: 100%;width: 100%;">
                            <p style="margin: 0;text-align:center;color:white">Sistemas de Bombeo<br />Proyecto, suministros e instalación</p>
                        </div>
                    </div>
                    <div class="col-4 col-12-narrower" style="height:150px;">
                        <div style="background-repeat: no-repeat;padding: 20.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/servicios/3.webp);height: 100%;width: 100%;">
                            <p style="margin: 0;text-align:center;color:white">Sistemas contra Incendio<br />Proyecto, suministros e instalación</p>
                        </div>
                    </div>
                </div>
            </section>
            </article>            
            <?php
				include 'pages/footer.php';
			?>
		</div>

		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/jquery.scrolly.min.js"></script>
		<script src="assets/js/jquery.scrollex.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
			
	</body>
</html>			
			