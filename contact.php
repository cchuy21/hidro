<!DOCTYPE HTML>
<html>
	<head>
		<title>Contacto | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="contact is-preload">
		<div id="page-wrapper">

		<?php
				$titulo="PROYECTOS";
				$show=0;
				$show2=0;
				$menu=array(array("Inicio","index",0),array("Servicios","servicios",0),array("Proyectos","proyectos",0),array("Equipo","equipo",0),array("Productos","productos",0),array("Contacto","contact",1));
				include 'pages/header.php';
			?>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon solid fa-envelope"></span>
						<h2>CONTACTANOS</h2>
					</header>

					<!-- One -->
						<section class="wrapper style4 special container medium">

							<!-- Content -->
								<div class="content">
									<form>
										<div class="row gtr-50">
											<div class="col-6 col-12-mobile">
												<input type="text" name="name" placeholder="NOMBRE" />
											</div>
											<div class="col-6 col-12-mobile">
												<input type="text" name="email" placeholder="CORREO" />
											</div>
											<div class="col-12">
												<input type="text" name="subject" placeholder="ASUNTO" />
											</div>
											<div class="col-12">
												<textarea name="message" placeholder="MENSAJE" rows="7"></textarea>
											</div>
											<div class="col-12">
												<ul class="buttons">
													<li><input type="submit" class="special" value="ENVIAR" /></li>
												</ul>
											</div>
										</div>
									</form>
								</div>

						</section>

				</article>

			<!-- Footer -->
				<footer id="footer">

					<ul class="icons">
						<li><a href="#" class="icon brands circle fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon brands circle fa-facebook-f"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon brands circle fa-google-plus-g"><span class="label">Google+</span></a></li>
						<li><a href="#" class="icon brands circle fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon brands circle fa-dribbble"><span class="label">Dribbble</span></a></li>
					</ul>

					<ul class="copyright">
						<li>&copy; Untitled</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
			</footer>
		</div>
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
	</body>
</html>