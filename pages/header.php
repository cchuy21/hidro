
<?php 
    if (!isset($show2)) {
        $show2=1;
        
    }
    if($show2){
        $class="alt";
    }else{
        $class="";
    }
?>
<header id="header" class="<?php echo $class; ?>">
    <h1 id="logo" style="font-weight:100"><a href="index.php"><img src="images/LOGO_HIDROCEN_c8mydg.webp" style="height:35px" /></a><p style="margin:0px"><b>IN</b>terés-<b>VO</b>luntad-<b>CO</b>nfianza</p></h1>
    <nav id="nav">
        <ul>
        <?php
            foreach($menu as $mn){
                if(!$mn[2]){
                    echo '<li><a href="'.$mn[1].'.php">'.$mn[0].'</a></li>';
                }else{
                    echo '<li class="current"><a href="'.$mn[1].'.php">'.$mn[0].'</a></li>';
                }
            }
        ?>
        </ul>
    </nav>
</header>
<?php
if($show2){
?>
<section id="banner">
    <div class="inner">
        <header>
            <?php
                echo '<h2>'.$titulo.'</h2>';
            ?>
        </header>
        <?php
        if($show){
        ?>
        <p>Creando <b>sustentabilidad</b><br /> a partir del tratamiento<br /> y <br />reuso del agua.</p>
        <footer>
            <ul class="buttons stacked">
                <li><a href="#main" class="button fit scrolly">Ver Más...</a></li>
            </ul>
        </footer>
        <?php
        }
        ?>
    </div>
</section>

<?php
}
?>
