<!DOCTYPE HTML>
<html>
	<head>
		<title>Productos | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="../images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
		<noscript><link rel="stylesheet" href="../assets/css/noscript.css" /></noscript>
	</head>
	<body class="index is-preload">
		<div id="page-wrapper">
			<!-- Main -->
			<article id="main">
				<header class="special container" style="padding-top:0px">
					<!--<span class="icon solid fa-chart-bar"></span>-->
					<h2><strong>Iniciar Sesión</strong></h2>
                    <br />
                    <form method="post" action="index.php">
                        <div class="row gtr-50">

                            <div class="col-4"></div>
                            <div class="col-4 col-12-mobile">
                                <input type="text" name="username" placeholder="USUARIO" />
                            </div>
                            <div class="col-4"></div>
                            <div class="col-4"></div>
                            <div class="col-4 col-12-mobile">
                                <input type="password" name="password" placeholder="CONTRASEÑA" />
                            </div>
                            <div class="col-4"></div>
                            <div class="col-12">
                                <ul class="buttons">
                                    <li><input type="submit" class="special" value="ENTRAR" /></li>
                                </ul>
                            </div>
                        </div>
                    </form>
				</header>
			</article>
		</div>
		<!-- Scripts -->
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/jquery.dropotron.min.js"></script>
		<script src="../assets/js/jquery.scrolly.min.js"></script>
		<script src="../assets/js/jquery.scrollex.min.js"></script>
		<script src="../assets/js/browser.min.js"></script>
		<script src="../assets/js/breakpoints.min.js"></script>
		<script src="../assets/js/util.js"></script>
		<script src="../assets/js/main.js"></script>
		<script>
			var c=0;
			jQuery(document).ready(()=>{
				jQuery(".dvimg2").off("click").on("click", function(){
					let vr = jQuery(this);
					if(!c){
						c++;
						jQuery("#pro").show();
					}
					jQuery('html, body').animate({
						scrollTop: $("#pr2").offset().top
					}, 2000);
					jQuery(".gen").hide("fast",function(){
						jQuery(".gene"+vr.data("val")).show("slow");
					});
				});
			});
		</script>
	</body>
</html>