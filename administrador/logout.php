<?php
session_start();
if(isset($_SESSION['loggin'])){
    unset($_SESSION['loggin']);
    session_destroy();
}
header('Location: index.php');

?>