<!DOCTYPE HTML>
<html>
	<head>
		<title>Productos | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="../images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
		<noscript><link rel="stylesheet" href="../assets/css/noscript.css" /></noscript>
        <style>
            tr{
                border-bottom: 1px solid #eee;
            }
            .modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
        </style>
	</head>
	<body class="index is-preload">
        <header id="header">
            <h1 id="logo" style="font-weight:100"><a target="_blank" href="../index.php"><img src="../images/LOGO_HIDROCEN_c8mydg.webp" style="height:35px" /></a><p style="margin:0px"><b>IN</b>terés-<b>VO</b>luntad-<b>CO</b>nfianza</p></h1>
            <nav id="nav">
                <ul>
                <li class="current"><a href="#">Servicios</a></li>
                <li><a href="#">proyectos</a></li>
                <li><a href="#">productos</a></li>
                <li><a href="logout.php">Salir</a></li>
                </ul>
            </nav>
        </header>
        <article id="main" style="padding-top:110px">
            <header class="special container" style="padding-top:0px">
                <div class="row" id="servicios">
                    <div class="col-12">
                        <h2><strong>SERVICIOS</strong></h2><button id="myBtn">Agregar</button><br /><br />
                        <div class="col-12" style="height:350px;overflow-y:auto;background:white;">
                            <table>
                                <thead>
                                    <tr style="font-weight:bold">
                                        <th>#</th>
                                        <th>Titulo</th>
                                        <th>Texto</th>
                                        <th>Imagen</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        for($i=1;$i<=40;$i++){
                                            echo '<tr>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td><button>Editar</button><button>Borrar</button></td>
                                                </tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </header>
        </article>
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <p>Some text in the Modal..</p>
            </div>
        </div>
	</body>
    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/jquery.dropotron.min.js"></script>
    <script src="../assets/js/jquery.scrolly.min.js"></script>
    <script src="../assets/js/jquery.scrollex.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>
    <script>
        jQuery(document).ready(()=>{
            // Get the modal
            var modal = jQuery("#myModal");
            var modal2 = document.getElementById("myModal");

            // Get the button that opens the modal
            var btn = jQuery("#myBtn");

            // Get the <span> element that closes the modal
            var span = jQuery(".close");

            // When the user clicks on the button, open the modal
            btn.onclick = function() {
                modal.style.display = "block";

            }
            btn.off("click").on("click",()=>{
                modal.fadeIn("slow");
            });
            span.off("click").on("click",()=>{
                modal.fadeOut("slow");

            });

            // When the user clicks on <span> (x), close the modal
            

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
            if (event.target == modal2) {
                modal.fadeOut("slow");
            }
            }
        });
    </script>
</html>