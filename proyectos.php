<?php
	include_once 'proyecto.php';
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Proyectos | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="index is-preload">
		<div id="page-wrapper">
			<?php
				$titulo="PROYECTOS";
				$show=0;
				$menu=array(array("Inicio","index",0),array("Servicios","servicios",0),array("Proyectos","proyectos",1),array("Equipo","equipo",0),array("Productos","productos",0),array("Contacto","contact",0));
				include 'pages/header.php';
			?>
			<!-- Main -->
			<article id="main">
				<header class="special container">
					<span class="icon solid fa-chart-bar"></span>
					<h2><strong>PROYECTOS</strong></h2>
					<br />
					<div class="row">
						<div class="col-8 col-12-narrower dvimg">
							<div style="background-repeat: no-repeat;padding: 80.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/proyectos/1.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">PROYECTOS CON EDAR</p>
							</div>
						</div>
						<div class="col-4 col-12-narrower dvimg">
							<div style="background-repeat: no-repeat;padding: 80.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/proyectos/2.jfif);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">PROYECTOS DE RIEGO Y JARDINERÍA</p>
							</div>
						</div>
					</div>
					<BR />
					<div class="row">
						<div class="col-4 col-12-narrower dvimg">
							<div style="background-repeat: no-repeat;padding: 80.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/proyectos/3.jfif);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">EQUIPOS DE BOMBEO</p>
							</div>
						</div>
						<div class="col-8 col-12-narrower dvimg">
							<div style="background-repeat: no-repeat;padding: 80.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/proyectos/4.jfif);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">OTROS PROYECTOS</p>
							</div>
						</div>
					</div>
				</header>
				<div id="pr2"></div>
				<section id="pro" style="display:none" class="wrapper style3 container special-alt">
					<div class="row gtr-50" style="text-align:center">
						<?php
							foreach($arr as $a){
								echo '<div class="col-12 col-12-narrower">
									<h2><strong>'.$a['titulo'].'</strong></h2>
									<p style="text-align:justify">'.$a['texto'].'</p>
								</div><br /><hr style="width: 80%;height: 1px;padding: 0;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));">';
							}
						?>
					</div>
				</section>

			</article>
			<?php
				include 'pages/footer.php';
			?>
		</div>
		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/jquery.scrolly.min.js"></script>
		<script src="assets/js/jquery.scrollex.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
		<script>
			var c=0;
			jQuery(document).ready(()=>{
				jQuery(".dvimg").off("click").on("click", ()=>{
					if(!c){
						c++;
						jQuery('html, body').animate({
							scrollTop: $("#pr2").offset().top
						}, 2000);
						jQuery("#pro").slideToggle( "slow", ()=>{
						});
					}
				});
			});
		</script>
	</body>
</html>