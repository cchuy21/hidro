<!DOCTYPE HTML>
<html>
	<head>
		<title>Hidrocen | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="index is-preload">
		<div id="page-wrapper">
			<?php
				$titulo="HIDROCEN";
				$show=1;
				$menu=array(array("Inicio","index",1),array("Servicios","servicios",0),array("Proyectos","proyectos",0),array("Equipo","equipo",0),array("Productos","productos",0),array("Contacto","contact",0));
				include 'pages/header.php';
			?>
			<!-- Main -->
			<article id="main">
				<header class="special container">
					<span class="icon solid fa-chart-bar"></span>
					<h2>ACERCA DE <strong>HIDROCEN</strong></h2>
					<p style="text-align:justify">Hidrodinámica del Bajío S.A. de C.V. es una empresa 100% Mexicana, de la rama Hidráulico Agro Industrial, ubicada en Aguascalientes, Aguascalientes. Con más de una década de experiencia, es una empresa que desarrolla y construye proyectos de innovación en la reutilización de recursos hídricos, algunos de éstos son: plantas tratadoras de agua, bombeos, conducción de agua, sistemas de riego, potabilización, invernaderos, entre otros.</p>
					<br />
					<p style="text-align:left">Buscamos:</p>
					<ul style="text-align:left;padding-left:50px">
						<li>Crear conciencia en la sociedad del uso adecuado del agua.</li>
						<li>Mejorar la eficiencia en menos espacio.</li>
						<li>Garantizar la calidad de nuestros productos.</li>
						<li>Generar ventajas competitivas para nuestros clientes.</li>
					</ul>
				</header>
				<!-- One -->
					<section class="wrapper style2 container special-alt">
						<div class="row gtr-50" style="text-align:center">
							<div class="col-6 col-12-narrower" style="height:150px;">
								<div style="background-repeat: no-repeat;padding: 50.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/1.webp);height: 100%;width: 100%;">
									<p style="margin: 0;">Estaciónes Depuradoras de Aguas Residuales</p>
								</div>	
							</div>
							<div class="col-6 col-12-narrower" style="height:150px;">
								<div style="background-repeat: no-repeat;padding: 50.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/2.webp);height: 100%;width: 100%;">
									<p style="margin: 0;">Potabilización</p>
								</div>
							</div>
							<div class="col-4 col-12-narrower" style="height:150px;">
								<div style="background-repeat: no-repeat;padding: 50.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/3.webp);height: 100%;width: 100%;">
									<p style="margin: 0;">Conducción de Agua</p>
								</div>
							</div>
							<div class="col-4 col-12-narrower" style="height:150px;">
								<div style="background-repeat: no-repeat;padding: 50.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/4.webp);height: 100%;width: 100%;">
									<p style="margin: 0;">Sistemas de Riego</p>
								</div>
							</div>
							<div class="col-4 col-12-narrower" style="height:150px;">
								<div style="background-repeat: no-repeat;padding: 50.6px 30px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/5.webp);height: 100%;width: 100%;">
									<p style="margin: 0;">Invernaderos</p>
								</div>
							</div>
							<!--<div class="col-8 col-12-narrower">

								<header>
									<h2>Behold the <strong>icons</strong> that visualize what you’re all about. or just take up space. your call bro.</h2>
								</header>
								<p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu comteger ut fermentum lorem. Lorem ipsum dolor sit amet. Sed tristique purus vitae volutpat ultrices. eu elit eget commodo. Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo.</p>
								<footer>
									<ul class="buttons">
										<li><a href="#" class="button">Find Out More</a></li>
									</ul>
								</footer>

							</div>
							<div class="col-4 col-12-narrower imp-narrower">

								<ul class="featured-icons">
									<li><span class="icon fa-clock"><span class="label">Feature 1</span></span></li>
									<li><span class="icon solid fa-volume-up"><span class="label">Feature 2</span></span></li>
									<li><span class="icon solid fa-laptop"><span class="label">Feature 3</span></span></li>
									<li><span class="icon solid fa-inbox"><span class="label">Feature 4</span></span></li>
									<li><span class="icon solid fa-lock"><span class="label">Feature 5</span></span></li>
									<li><span class="icon solid fa-cog"><span class="label">Feature 6</span></span></li>
								</ul>

							</div>-->
						</div>
					</section>
				<!-- Two -->
					<section class="wrapper style1 container special">
						<div class="row">
							<div class="col-12 col-12-narrower" style="text-align:center">
								<h2>¿CÓMO LO HACEMOS?</h2>
								<h5>Nuestro proceso para el saneamiento y reutilización del agua</h5>
							</div>
						</div>
						<br />
						<div class="row" id="rw">
							<div class="col-1 col-12-narrower">
							</div>
							<div class="col-2 col-12-narrower">
								<div style="height: 50px;width: 50px;border: solid 1px;margin: 0 auto 0;padding: 10px;">
									1
								</div>
								Captación
								<ul>
									<li>Pozos de Visita</li>
									<li>Bombeo</li>
									<li>Conducción</li>
								</ul>
							</div>
							<div class="col-2 col-12-narrower">
								<div style="height: 50px;width: 50px;border: solid 1px;margin: 0 auto 0;padding: 10px;">
									2
								</div>
								
								Tratamiento
								<ul>
									<li>Tratamiento Biológico con Biopelícula hidro activada</li>
									<li>Tratamiento Mixto</li>
								</ul>
							</div>
							<div class="col-2 col-12-narrower">
								<div style="height: 50px;width: 50px;border: solid 1px;margin: 0 auto 0;padding: 10px;">
									3
								</div>
								Filtrado
								<ul>
									<li>Filtrado previo y posterior</li>
								</ul>
							</div>
							<div class="col-2 col-12-narrower">
								<div style="height: 50px;width: 50px;border: solid 1px;margin: 0 auto 0;padding: 10px;">
									4
								</div>
								Distribución
								<ul>
									<li>Cisternas</li>
									<li>Sistemas de Riego contra Incendios</li>
									<li>Bombeo</li>
								</ul>
							</div>
							<div class="col-2 col-12-narrower">
								<div style="height: 50px;width: 50px;border: solid 1px;margin: 0 auto 0;padding: 10px;">
									5
								</div>
								Jardinería
							</div>
							<div class="col-1 col-12-narrower">
							</div>
						</div>
						<!--<div class="row">
							<div class="col-4 col-12-narrower">
								<section>
									<span class="icon solid featured fa-check"></span>
									<header>
										<h3>This is Something</h3>
									</header>
									<p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.</p>
								</section>

							</div>
							<div class="col-4 col-12-narrower">

								<section>
									<span class="icon solid featured fa-check"></span>
									<header>
										<h3>Also Something</h3>
									</header>
									<p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.</p>
								</section>

							</div>
							<div class="col-4 col-12-narrower">

								<section>
									<span class="icon solid featured fa-check"></span>
									<header>
										<h3>Probably Something</h3>
									</header>
									<p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo suscipit dolor nec nibh. Proin a ullamcorper elit, et sagittis turpis. Integer ut fermentum.</p>
								</section>

							</div>
						</div>-->
					</section>
				<!-- Three -->
					<section class="wrapper style3 container special">
						<div class="row">
							<div class="col-12 col-12-narrower" style="text-align:center">
								<h2>CLIENTES</h2>
								<h5>Da clic en el logo para conocer más el proyecto</h5>
							</div>
						</div>
						<div class="row" id="img">
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/heb.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/meridiam.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/uaa.gif" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/ags.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/cantia.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/sac.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/inegi.webp" /></div>
							<div class="col-2 col-12-narrower" style="padding-top: 70px;"><img style="width:100%;height:auto" src="images/empresas/gcp.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/uvm.webp" /></div>
							<div class="col-2 col-12-narrower" style="padding-top: 80px;"><img style="width:100%;height:auto" src="images/empresas/hyline.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/r.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/gpd.webp" /></div>
							<div class="col-2 col-12-narrower"><img style="width:100%;height:auto" src="images/empresas/qua.gif" /></div>
							<div class="col-2 col-12-narrower" style="padding-top: 55px;"><img style="width:100%;height:auto" src="images/empresas/urbik.webp" /></div>
						</div>
						<!--
						<header class="major">
							<h2>Next look at this <strong>cool stuff</strong></h2>
						</header>
						<div class="row">
							<div class="col-6 col-12-narrower">

								<section>
									<a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
									<header>
										<h3>A Really Fast Train</h3>
									</header>
									<p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
								</section>

							</div>
							<div class="col-6 col-12-narrower">

								<section>
									<a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
									<header>
										<h3>An Airport Terminal</h3>
									</header>
									<p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
								</section>

							</div>
						</div>
						<div class="row">
							<div class="col-6 col-12-narrower">

								<section>
									<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
									<header>
										<h3>Hyperspace Travel</h3>
									</header>
									<p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
								</section>

							</div>
							<div class="col-6 col-12-narrower">

								<section>
									<a href="#" class="image featured"><img src="images/pic04.jpg" alt="" /></a>
									<header>
										<h3>And Another Train</h3>
									</header>
									<p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
								</section>

							</div>
						</div>
						<footer class="major">
							<ul class="buttons">
								<li><a href="#" class="button">See More</a></li>
							</ul>
						</footer>
						-->
					</section>
			</article>
			<!-- CTA -->
			<!--<section id="cta">

				<header>
					<h2>Ready to do <strong>something</strong>?</h2>
					<p>Proin a ullamcorper elit, et sagittis turpis integer ut fermentum.</p>
				</header>
				<footer>
					<ul class="buttons">
						<li><a href="#" class="button primary">Take My Money</a></li>
						<li><a href="#" class="button">LOL Wut</a></li>
					</ul>
				</footer>

			</section>-->
			<?php
				include 'pages/footer.php';
			?>
		</div>
		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/jquery.scrolly.min.js"></script>
		<script src="assets/js/jquery.scrollex.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>