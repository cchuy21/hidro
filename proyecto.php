<?php
	$arr=array();
	$ar = array();
	$ar["titulo"] = "Plaza Tres Centurias";
	$ar["texto"] = "Diseño e instalación de sistema de riego y jardinería en el complejo.<br />Se realizó en el año 2005. Se tomó la decisión de un sistema de riego automático, ya que fueron los primero equipos automatizados. Anteriormente se utilizaba el sistema de válvulas y mangueras. Haciendo mas tediosa la función de la jardinería pública";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Instituto Nacional del Deporte";
	$ar["texto"] = "Rehabilitación del campo de fútbol.<br />Se realizó en el año 2006. Su finalidad fue realizar un campo deportivo duradero y para un alto uso, ya que se utilizó un extracto de suelo arenoso y un sistema de riego fijo para hacer más útil el tiempo de vida de una cancha deportiva y de esta manera lograr un alto impacto de ella. En la elaboración de este sistema de riego se cumple con los estándares internacionales del deporte en césped."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Camellón Canal Interceptor";
	$ar["texto"] = "Diseño e instalación de sistema de riego con una longitud de 3 kilómetros.<br />Fueron los primeros sistemas de riego automáticos en camellones en el municipio de Aguascalientes, en el sexenio de Felipe González. Procurando ahorro de agua, la preservación de las áreas verde, disminución de contaminación y reducción de costos de mano de obra."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Campo Deportivo de Beisbol Profesional Romo Chavez";
	$ar["texto"] = "Diseño y construcción, incluidas las labores de automatización de riego, adaptación de zona de enraizamiento, colocación de césped deportivo y mantenimiento del área.<br />Se realizó en el sexenio de Felipe González. Se restauró la liga profesional de béisbol en el estado de Aguascalientes por lo que se invirtió con un gran interés el campo deportivo Romo Chávez, pero principalmente tuvo un cambio sustancial en el ámbito como campo deportivo, ya que en el siglo XXI los campos evolucionaron imitando la calidad en el ámbito internacional. Por esta razón se decidió una renovación total en este rubro."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Universidad del Valle de México";
	$ar["texto"] = "Diseño y construcción del riego automatizado, incluye implementación de zona de enraizamiento y colocación de césped deportivo, así como el mantenimiento del mismo.<br />La universidad Valle de México se preocupó por tener una cancha de futbol en excelentes condiciones para el ámbito deportivo de sus alumnos."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Avenida José Gómez Morín";
	$ar["texto"] = "Una de las avenidas más funcionales y cónicas en el municipio de Aguascalientes. Fueron los primeros embellecimientos urbanos en este estado. Por esta razón se realizó el sistema de riego."; 
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = "Avenida Pocitos";
	$ar["texto"] = "Inicios de modernización en sistemas de riego en el municipio de Jesús María."; 
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = "Parque Lineal en el Fraccionamiento Real de Haciendas";
	$ar["texto"] = "A cargo de la constructora Gig Desarrollos Inmobiliarios. Consta de 3000 metros cuadrados, construcción y colocación de adocreto, guarniciones, áreas de recreo, escaleras, canchas de futbol rápido y guarderías, dos canchas de basquetbol, muros de contención, instalación de riego automatizado y colocación de césped en 8000 metros de areas verdes. Construcción de 2 cisternas de 20 metros cúbicos.<br />El inicio en automatización en riego en Línea Verde. Se preocupó por mantener en condiciones óptimas sus áreas verdes y opto por la instalación de un sistema de riego que cumpliera con ello."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "2da Sección del Parque Lineal en el Fraccionamiento Real de Haciendas";
	$ar["texto"] = "A cargo de la constructora Gig Desarrollos Inmobiliarios, Consta de 1400 metros cuadrados de adocreto, guarniciones, áreas de recreo, escaleras, graderías, 2 canchas de basquetbol con muros de contención, instalación de riego automatizado y colocación de césped en 5000 metros de areas verdes. Construcción de 2 cisternas de 20 metros cúbicos.<br />Se extendió el sistema de riego en césped, dado el éxito de la primera."; 
	array_push($arr, $ar);
			
	$ar = array();
	$ar["titulo"] = "3ra Sección del Parque Lineal en el Fraccionamiento Real de Haciendas";
	$ar["texto"] = "A cargo de la constructora Gig Desarrollos Inmobiliarios, Consta de 1400 metros cuadrados de adocreto, guarniciones, áreas de recreo, escaleras, graderías, 2 canchas de basquetbol con muros de contención, instalación de riego automatizado y colocación de césped en 5000 metros de areas verdes. Construcción de 2 cisternas de 20 metros cúbicos.<br />Dado el éxito de la segunda sección se extendió el sistema de riego en este fraccionamiento."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Tienda Departamental Cantia";
	$ar["texto"] = "Implementación de área verde y riego automatizado, así como la colocación de césped.<br />La iniciativa privada comienza a preocuparse por la automatización en su irrigación de sus áreas verdes."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Instituto Nacional de Estadística y Geografía (INEGI)";
	$ar["texto"] = "Licitación pública nacional número 06131013-002 ordenador.<br />Implementación de área verde y riego automatizado, así como la colocación de césped.<br />Se licito y se ganó un proyecto de gran extensión para el ahorro en el consumo de agua de las áreas verdes."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Residencial "Ruscello"';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización del sistema de riego en cada uno de los lotes, así como en todos los camellones y en las áreas de recreo y de donación.<br />Diseño, desarrollo y construcción de <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR).</a> Capacidad de 5.0 LPS y Cisterna de Almacenamiento de Aguas Tratadas.<br />Diseño y construcción de filtro para agua potable del fraccionamiento con un alto contenido de Fe, Ca, y Mg, para un gasto de 36 LPS.<br />Mantenimiento de áreas verdes, sistema de riego y <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Agua Residual (EDAR).</a><br />Su finalidad fue el desarrollo, implementación y ai¡utomatización del sistema de riego en cada uno de los lotes, así como todos los camellones y en las áreas verdes de recreo y de donación."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Casa de los Sacerdotes "Legionarios de Cristo"';
	$ar["texto"] = "Diseño, desarrollo y construcción de la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR)</a><br />La primer planta de tratamiento experimental con la innovación de lecho fijo y desinfección con ozono. Se realizó en el año 2009. En este año se decidió elaborar plantas de tratamiento."; 
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = 'Empresa "Empacadora San Francisco S.A. de C.V."';
	$ar["texto"] = "Diseño, desarrollo, y construcción de la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR)</a><br />Inicio de estación depuradora de agua residual en el ámbito industrial. Se comenzó por tratar a la industria a tener una mejora en sus procesos."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Residencial "Santa Paulina"';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización del sistema de riego en áreas de recreo y de donación.<br />El inicio de la red de distribución de agua para las aguas verdes. Diseño y construcción de riego para las áreas verdes aun en existencia y con un funcionamiento del 100% al tiempo actual. En el año 2000 fue realizada. Se plantó 7 hectáreas de pasto en rollo."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Parque Lago de la Feria de San Marcos para el Gobierno del Estado de Aguascalientes';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización de sistemas de riego por goteo en jardines, <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR)</a>, capacidad de 0.7 LPS; así como diseño de jardinería."; 
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = 'Mantenimiento al Parque Lago de la Feria de San Marcos';
	$ar["texto"] = "Mantenimiento de sistema de riego y programación, riego de pastos y jardineras, poda de pastos, poda de plantas de seto y ornamentales, poda de formación de árboles, deshierbe, cajeteo y labranza de jardineras, limpieza y papeleo y reposición de bolsas, plantación cuando se programe hacerlo, fumigación y fertilización, limpieza del parque incluye: andadores, área de juegos infantiles (desinfección del área), foro de lago, áreas verdes, y áreas de gimnasio de adultos, limpieza y cuidado preventivo de celdas fotovoltaicas, mantenimiento y limpieza del lago, aireador, difusores, fuertes.<br />En el año 2014. Este proyecto fue un éxito. Este parque ya lo había realizado varias veces y quedo concluido hasta que Hidrodinámica del Bajío lo elaboro. Con la finalidad del aprovechamiento de las aguas negras de la colonia las Flores para la utilización y aprovechamiento de las áreas verdes urbanas en los parques púbicos. Así como para el aguas del lago de la Feria."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'EDAR Dulces Karla';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización de la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR).</a> Capacidad de 0.5 LPS.<br />Preocupación del tratamiento y reutilización de las aguas negras. Se realizó la planta de tratamiento que hasta la actualidad sigue en funcionamiento. Esta planta beneficio el tratamiento de las aguas negras en disminución de contaminación."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Cerrito de la Cruz';
	$ar["texto"] = "Diseño e instalación de sistema de riego automatizado y equipos de bombeo.<br />Su propósito fue embellecer y establecer con recursos estatales el cerrito de la cruz. Se hizo un sistema de riego automatizado que se alimentaba de la plata de tratamiento del Parque CEDAZO."; 
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = 'Foro Trece Oriente';
	$ar["texto"] = "Diseño e instalación de sistema de riego automatizado y equipos de bombeo.<br />Se realizó sistema de riego y apoyo en la obra pública. Con la finalidad de la automatización en las áreas verdes."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Centro Productor de Árboles';
	$ar["texto"] = "Rehabilitación de cuatro invernaderos, instalación de sistema de fertirrigación, sistema de riego automático con robot, sistema de riego tradicional y equipo de bombeo.<br />Fue la rehabilitación de los invernaderos del estado. Se instalaron robots para el riego y equipos de micro aspersión en soleaderos para el desarrollo de árboles."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Bosque Urbano';
	$ar["texto"] = "Diseño e instalación de sistema de riego automático.<br />Su finalidad fue brindar apoyo en sistema de riego automático."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Tierra Verde';
	$ar["texto"] = "Construcción de pozo de visita y colocación del equipo de bombeo.<br />Existía una competencia elevada en plantas de tratamiento. A pesar de ello fue otorgada la realización a Hidrodinámica del Bajío. Alimenta todas las áreas verdes. Fue un éxito rotundo. Se realizó un cárcamo de bombeo, luego la planta de tratamiento y la inyección de agua tratada dentro del fraccionamiento para la utilización en las áreas verdes comunes del Fraccionamiento Tierra Verde."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Tienda HEB';
	$ar["texto"] = "Diseño e instalación de sistema de riego automático y jardinería.<br />El primer se realizó el proyecto en Aguascalientes, luego el proyecto de León y finalmente el proyecto en San Luis. La finalidad fue dar apoyo técnico en jardinería y riego técnico mediante la implementación de <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR)</a>"; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Parque Lago de la Feria de San Marcos';
	$ar["texto"] = "Colocación de geomembrana de PVC del Lago del Parque de la Feria"; 
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = 'Hotel La Quinta Inn';
	$ar["texto"] = "Diseño, desarrollo, y construcción de la la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR).</a> Capacidad de 0.5 LPS.<br />Fue la primer planta de tratamiento en hoteles fuera de la ciudad urbana."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = '2da Etapa. Tierra Verde';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización de la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR).</a> Capacidad de 2.5 LPS."; 
	array_push($arr, $ar);
		
	$ar = array();
	$ar["titulo"] = 'MECA';
	$ar["texto"] = "Suministro e instalación de equipo de velocidad variable con bomba centrífuga."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'DIF - Mahatma Ghandi';
	$ar["texto"] = "Suministro e instalación de equipo de velocidad variable con bomba centrífuga.<br />Planta de tratamieto en una localidad de puerta de fraguas en el municipio de Calvillo Aguascalientes, la importancia de esta obra fue dejar de verter 84.6 m3 de aguas residuales a la presa adjuta a la localidad."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Municipio de Calvillo';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización de la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR).</a> Capacidad de 1.0 LPS."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Ciudad Justicia';
	$ar["texto"] = "Diseño, desarrollo, implementación y automatización de la <a href='https://www.hidrocen.com/productos#3' taget='_blank'>Estación Depuradora de Aguas Residuales (EDAR).</a> Capacidad de 0.5 LPS.<br />Solicitud de del Gobierno del Estado de Aguascalientes para el servicio de depuracion de agus en oficinas publicas en el Estado."; 
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = 'Municipio de Calvillo';
	$ar["texto"] = "Ampliación de la red de alcantarillado, telesecundaria, para la comunidad de Puerta de Fragua, Calvillo.<br />Sistemas de riego en el jardin de Municipio de Calvillo Ags."; 
	array_push($arr, $ar);

?>