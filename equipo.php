<?php
	include_once 'producto.php';
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Productos | Hidrodinámcia del Bajio</title>
		<meta charset="utf-8" />
		<link href="images/isotipo_u0cgbo.png" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="index is-preload">
		<div id="page-wrapper">
			<?php
				$titulo="PRODUCTOS";
				$show=0;
				$menu=array(array("Inicio","index",0),array("Servicios","servicios",0),array("Proyectos","proyectos",0),array("Equipo","equipo",0),array("Productos","productos",1),array("Contacto","contact",0));
				include 'pages/header.php';
			?>
			<!-- Main -->
			<article id="main">
				<header class="special container">
					<span class="icon solid fa-chart-bar"></span>
					<h2><strong>PRODUCTOS</strong></h2>
					<br />
					<div class="row">
						<div class="col-3 col-12-narrower dvimg2" data-val="0">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/1.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Estación depuradora de aguas residuales (EDAR)</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="1">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/2.jfif);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Prototipo EDAR Modular</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="2">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/3.jfif);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Proyecto LEGO</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="3">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/4.jfif);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Sistemas de Riego</p>
								<p style="margin: 0;color:white; font-size:8pt">Suministro y refacciones</p>
							</div>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-3 col-12-narrower dvimg2" data-val="4">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/5.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Sistemas de Bombeo</p>
								<p style="margin: 0;color:white; font-size:8pt">Suministro y refacciones</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="5">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/6.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Sistemas contra Incendios</p>
								<p style="margin: 0;color:white; font-size:8pt">Suministro y refacciones</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="6">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/7.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Conducción de Agua</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="7">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/8.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Jardinería</p>
							</div>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-3 col-12-narrower dvimg2" data-val="8">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/9.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Invernaderos</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="9">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/10.webp);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Robots de Riego</p>
							</div>
						</div>
						<div class="col-3 col-12-narrower dvimg2" data-val="10">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/11.jpg);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Ozonificador</p>
							</div>
						</div>
						<div data-val="11" class="col-3 col-12-narrower dvimg2">
							<div style="background-repeat: no-repeat;padding: 30.6px 10px;background-size: cover;background-color: transparent;background-position: 50% 50%;background-image:linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url(images/productos/12.jpg);height: 100%;width: 100%;">
								<p style="margin: 0;color:white;">Electrocoagulador</p>
							</div>
						</div>
					</div>
				</header>
				<div id="pr2"></div>
				<section id="pro" style="display:none" class="wrapper style3 container special-alt">
					
						<?php
							foreach($arr as $k=>$a){
								echo '
								<div class="row gtr-50 gen gene'.$k.'" style="text-align:center; display:none">
									<div class="col-12 col-12-narrower">
									<h3><strong>'.$a['titulo'].'</strong></h3>
									<div class="row">
										<div class="col-5 col-12-narrower">
											'.$a['video'].'VIDEO O IMAGEN
										</div>
										<div class="col-7 col-12-narrower">
											<p style="text-align:justify">'.$a['texto'].'</p>
										</div>
									</div>
									<div class="col-12 col-12-narrower">
										<button style="BORDER-RADIUS: 10PX;">DESCARGAR FICHA TÉCNICA</button>
										<br /><br />
										<button style="BORDER-RADIUS: 10PX; text-transform: uppercase;">VER PROYECTOS</button>
									</div>
								</div>
							</div>';
							}
						?>
					
				</section>

			</article>
			<?php
				include 'pages/footer.php';
			?>
		</div>
		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/jquery.scrolly.min.js"></script>
		<script src="assets/js/jquery.scrollex.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
		<script>
			var c=0;
			jQuery(document).ready(()=>{
				jQuery(".dvimg2").off("click").on("click", function(){
					let vr = jQuery(this);
					if(!c){
						c++;
						jQuery("#pro").show();
					}
					jQuery('html, body').animate({
						scrollTop: $("#pr2").offset().top
					}, 2000);
					jQuery(".gen").hide("fast",function(){
						jQuery(".gene"+vr.data("val")).show("slow");
					});
				});
			});
		</script>
	</body>
</html>