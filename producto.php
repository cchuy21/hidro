<?php
	$arr=array();
	$ar = array();
	$ar["titulo"] = "Estación Depuradora de Aguas Residuales (EDAR)";
	$ar["texto"] = "El sistema HIDROCEN® implementa un sistema vanguardista en una Estación Depuradora de Aguas Residuales (EDAR), con sistema BHA (Biopelícula Hidro Activada), la cual es ahorradora, eficiente y produce agua incolora e inodora. Una de las ventajas de la EDAR HIDROCEN® es que ésta no produce gases tóxicos gracias a los procesos biológicos aerobios. Construimos plantas que se mimetizan con el entorno, resultando en una planta eficiente e imperceptible.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Prototipo EDAR Modular";
	$ar["texto"] = "Modelo de EDAR HIDROCEN® de 1 L/s.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Proyecto LEGO";
	$ar["texto"] = "Modelo de EDAR HIDROCEN® de 1 L/s.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Sistemas de Riego";
	$ar["texto"] = "En Hidrodinámica del Bajío tenemos el sistema de riego automatizado que se adapta a las necesidades de cualquier proyecto, desde el jardín trasero de una casa, hasta un parque de numerosas hectáreas. Contamos con diferentes tipos de sistemas de riego, así como complementos para hacer más eficiente su funcionamiento.<br />
	<ul id='lista'>
		<li>Riego con aspersores</li>
		<li>Riego con difusores</li>
		<li>Riego por goteo</li>
		<li>Riego con manguera porosa</li>
		<li>Riego con microaspersores</li>
		<li>Riego con manguera y válvula de acoplamiento rápido</li>
	</ul>";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Sistemas de Bombeo";
	$ar["texto"] = "Contamos con equipos a la venta, así como personal capacitado para la instalación de un sistema de bombeo, cumpliendo los requisitos y estándares de calidad y operación en cualquier instalación, ya sea para la red sanitaria y/o sus procesos.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = "Sistemas contra Incendios";
	$ar["texto"] = "Contamos con equipos a la venta, así como personal capacitado para la instalación de un sistema contra incendios, cumpliendo los requisitos y estándares de calidad y operación en cualquier instalación, ya sea para la red hidráulica y/o sus procesos.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = "Conducción de Agua";
	$ar["texto"] = "Las tuberías se clasifican por el tipo de fluido que conducen, según el fluido se identifica el tipo de tubería, marca, conexión a utilizar.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = "Jardinería";
	$ar["texto"] = "En Hidrodinámica sabemos el valor del paisaje, pues proporciona el primer impacto visual y una imagen positiva. Por nuestra parte ofrecemos el diseño, instalación y mantenimiento de áreas verdes.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Invernaderos";
	$ar["texto"] = "Desarrollamos el proyecto completo, desde el diseño, la construcción y montaje de la estructura, el sistema de riego, la automatización y fertilizantes.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);
	
	$ar = array();
	$ar["titulo"] = "Robots de Riego";
	$ar["texto"] = "FalconRain es un sistema de riego que consta de:<br /><br />Un robot de riego con funciones automáticas ida y regreso continuo, para satisfacer el riego requerido por el cultivo.<br />Puente/Aguilón móvil, a base de perfil de acero galvanizado de dimensión entre 9.30 y 10.20 m, considerando esta dimensión total del robot, cuenta con una altura ajustable sobre el piso de 1.40 a 1.70 m, según las necesidades del usuario.</p>
	<br /><ul>
	<li>Sistema de aspersión con un consumo de agua total de 1.2 L/s, considerando este consumo por ambos aguilones.</li>
	<li>Mecanismo de tracción con moto reductor.</li>
	<li>Sistema de retorno automático.</li>
	</ul>
	<br />
	<p>Avance sobre rieles de perfil de acero galvanizado, montados al piso, donde el robot se desplaza libremente satisfaciendo los requerimientos del cultivo y regando a su vez el área necesaria.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	$ar = array();
	$ar["titulo"] = "Ozonificador";
	$ar["texto"] = "Se integra en el sistema EDAR como un proceso que desinfecta el agua tratada; el ozono se obtiene por la ionización del oxígeno presente en el aire, y se dosifica a las concentraciones que las distintas aplicaciones requieran. Nuestro fabricado de ozonificadores cumple con la norma internacional del mercado.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);

	
	$ar = array();
	$ar["titulo"] = "Electrocoagulador";
	$ar["texto"] = "El equipo de electrocoagulación HIDROCEN® permite tratar las aguas residuales en continuo, sin tener que almacenarlas. Es un complemento al tratamiento primario del agua residual industrial en una EDAR.";
	$ar["ficha"] = "";
	$ar["video"] = "";
	array_push($arr, $ar);
?>